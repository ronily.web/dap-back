const router = require("express").Router();
const controller = require("./controller");

router.get("/cultures", controller.getAll);
router.get("/cultures/:id", controller.getByPk);
router.post("/cultures", controller.add);
router.put("/cultures/:id", controller.update);
router.delete("/cultures/:id", controller.delete);

module.exports = router;
