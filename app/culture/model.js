"use strict";

module.exports = (sequelize, DataTypes) => {
  const Culture = sequelize.define("Culture", {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Culture.associate = function(models) {
    Culture.belongsToMany(models.Farmer, {
      through: 'PlantationCulture',
      as: 'planters',
      foreignKey: 'culture_id'
    });
    Culture.belongsToMany(models.Farmer, {
      through: 'SalesCulture',
      as: 'sellers',
      foreignKey: 'CultureId'
    });
  };
  return Culture;
};
