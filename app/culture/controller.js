const { Culture } = require("../sequelize");

module.exports = {
  // lista as culturas
  getAll(req, res) {
    return Culture
      .findAll({
        attributes: ['id', 'name']
      })
      .then((cultures) => res.status(200).send(cultures))
      .catch((error) => res.status(200).send(error))
  },

  // busca uma cultura pela sua chave primaria
  getByPk(req, res) {
    return Culture
      .findByPk(req.params.id, {
        attributes: ['name']
      })
      .then((culture) => res.status(200).send(culture))
      .catch((error) => res.status(200).send(error))
  },

  // adiciona uma cultura
  add(req, res) {
    return Culture
      .create({
        name: "culture"
      })
      .then((culture) => res.status(201).send(culture))
      .catch((error) => res.status(400).send(error))
  },

  //atualiza uma cultura
  update(req, res) {
    return Culture
      .findByPk(req.params.id)
      .then(culture => {
        if (!culture) {
          return res.status(404).send({
            message: 'Culture Not Found',
          });
        }
        return culture
          .update({
            name: req.body.name,
          })
          .then((culture) => res.status(200).json(culture))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  //remove uma cultura
  delete(req, res) {
    return Culture
      .findByPk(req.params.id)
      .then((culture) => {
        if (!culture) {
          return res.status(404).send({
            message: 'Culture Not Found',
          });
        }
        return culture
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }
};
