const router = require("express").Router();
const controller = require("./controller");

router.get("/farmers", controller.getAll);
router.get("/farmers/:id", controller.getByPk);
router.put("/farmers/:id", controller.update);
router.delete("/farmers/:id", controller.delete);
router.post("/farmers", controller.addWithPlantationsAndSales);
router.delete("/farmers/:id/plantations", controller.removePlantations);
router.put("/farmers/:id/plantations", controller.updatePlantations);
router.delete("/farmers/:id/sales", controller.removeSales);
router.put("/farmers/:id/sales", controller.updateSales);

module.exports = router;
