"use strict";

module.exports = (sequelize, DataTypes) => {
  const Farmer = sequelize.define("Farmer", {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    cpf: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  Farmer.associate = function(models) {
    Farmer.belongsTo(models.Address, {
      foreignKey: 'address_id',
      as: 'address',
      onDelete: 'CASCADE'
    });
    Farmer.belongsToMany(models.Culture, {
      through: 'PlantationCulture',
      as: 'plantations',
      foreignKey: 'farmer_id'
    });
    Farmer.belongsToMany(models.Culture, {
      through: 'SaleCulture',
      as: 'sales',
      foreignKey: 'farmer_id'
    });
  };
  return Farmer;
};
