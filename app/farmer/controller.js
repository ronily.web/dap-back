const { Farmer, Culture, Address } = require("../sequelize");

module.exports = {
  // lista os agricultores
  getAll(req, res) {
    return Farmer
      .findAll({
        include: [
          {
            model: Address,
            as: 'address',
            attributes: ['county', 'area', 'proximity']
          },
          {
            model: Culture,
            as: 'plantations',
            attributes: ['name'],
            through: { attributes: [] }
          },
          {
            model: Culture,
            as: 'sales',
            attributes: ['name'],
            through: { attributes: [] }
          },
        ],
        attributes: ['id', 'name', 'cpf']
      })
      .then((farmers) => res.status(200).send(farmers))
      .catch((error) => res.status(200).send(error))
  },

  // busca um agricultor pela sua chave primaria
  getByPk(req, res) {
    return Farmer
      .findByPk(req.params.id, {
        include: [
          {
            model: Address,
            as: 'address',
            attributes: ['county', 'area', 'proximity']
          },
          {
            model: Culture,
            as: 'plantations',
            attributes: ['id'],
            through: { attributes: [] }
          },
          {
            model: Culture,
            as: 'sales',
            attributes: ['id'],
            through: { attributes: [] }
          },
        ],
        attributes: ['name', 'cpf']
      })
      .then((farmer) => res.status(200).send(farmer))
      .catch((error) => res.status(200).send(error))
  },

  // adiciona um agricultor com seu endereço, suas plantações e suas vendas
  addWithPlantationsAndSales(req, res) {
    return Address
      .create({
        county: req.body.county,
        area: req.body.area,
        proximity: req.body.proximity
      })
      .then((address) => {
        Farmer
          .create({
            name: req.body.name,
            cpf: req.body.cpf,
            address_id: address.id
          })
          .then((farmer) => {
            Culture
              .findAll({
                where: { id: req.body.plantations }
              })
              .then((cultures) => {
                if (!cultures) {
                  return res.status(404).send({
                    message: 'Culture Not Found',
                  });
                }
                farmer.addPlantations(cultures);
              });
            Culture
              .findAll({
                where: { id: req.body.sales }
              })
              .then((cultures) => {
                if (!cultures) {
                  return res.status(404).send({
                    message: 'Culture Not Found',
                  });
                }
                farmer.addSales(cultures);
                return res.status(200).send(farmer);
              })
          })
          .catch((error) => res.status(400).send(error))
      })
      .catch((error) => res.status(400).send(error));
  },

  // atualiza um agricultor
  update(req, res) {
    console.log(req.body);
    return Farmer
      .findByPk(req.params.id)
      .then((farmer) => {
        if (!farmer) {
          return res.status(404).send({
            message: 'Farmer Not Found',
          });
        }
        Address
          .findByPk(farmer.address_id)
          .then((address) => {
            address.update({
              county: req.body.county,
              area: req.body.area,
              proximity: req.body.proximity
            })
          })
          .catch((error) => res.status(400).send(error));
        Culture
          .findAll({
            where: { id: req.body.plantations }
          })
          .then((cultures) => {
            if (!cultures) {
              return res.status(404).send({
                message: 'Culture Not Found',
              });
            }
            farmer.setPlantations(cultures);
          });
        Culture
          .findAll({
            where: { id: req.body.sales }
          })
          .then((cultures) => {
            if (!cultures) {
              return res.status(404).send({
                message: 'Culture Not Found',
              });
            }
            farmer.setSales(cultures);
          });
        farmer.update({
            name: req.body.name,
            cpf: req.body.cpf
          })
          .then(() => res.status(200).json(farmer))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  //remove um agricultor
  delete(req, res) {
    return Farmer
      .findByPk(req.params.id)
      .then((farmer) => {
        if (!farmer) {
          return res.status(404).send({
            message: 'Farmer Not Found',
          });
        }
        Address
          .findByPk(farmer.address_id)
          .then((address) => {
            address.destroy()
              .then(() => {
                farmer
                  .destroy()
                  .then(() => res.status(204).send())
                  .catch((error) => res.status(400).send(error));
              })
          });
      })
      .catch((error) => res.status(400).send(error));
  },

  // atualiza o conjunto de plantações
  updatePlantations(req, res) {
    return Farmer
      .findByPk(req.params.id)
      .then((farmer) => {
        if (!farmer) {
          return res.status(404).send({
            message: 'Farmer Not Found',
          });
        }
        Culture
          .findAll({
            where: { id: req.body.plantations }
          })
          .then((cultures) => {
            if (!cultures) {
              return res.status(404).send({
                message: 'Culture Not Found',
              });
            }
            farmer.setPlantations(cultures);
            return res.status(200).send(farmer);
          })
      })
      .catch((error) => res.status(400).send(error));
  },

  // remove um conjunto de plantações
  removePlantations(req, res) {
    return Farmer
      .findByPk(req.params.id)
      .then((farmer) => {
        if (!farmer) {
          return res.status(404).send({
            message: 'Farmer Not Found',
          });
        }
        Culture
          .findAll({
            where: { id: req.body.plantations }
          })
          .then((cultures) => {
            if (!cultures) {
              return res.status(404).send({
                message: 'Culture Not Found',
              });
            }
            farmer.removePlantations(cultures);
            return res.status(200).send(farmer);
          })
      })
      .catch((error) => res.status(400).send(error));
  },

  // atualiza o conjunto de vendas
  updateSales(req, res) {
    return Farmer
      .findByPk(req.params.id)
      .then((farmer) => {
        if (!farmer) {
          return res.status(404).send({
            message: 'Farmer Not Found',
          });
        }
        Culture
          .findAll({
            where: { id: req.body.sales }
          })
          .then((cultures) => {
            if (!cultures) {
              return res.status(404).send({
                message: 'Culture Not Found',
              });
            }
            farmer.setSales(cultures);
            return res.status(200).send(farmer);
          })
      })
      .catch((error) => res.status(400).send(error));
  },

  // remove um conjunto de vendas
  removeSales(req, res) {
    return Farmer
      .findByPk(req.params.id)
      .then((farmer) => {
        if (!farmer) {
          return res.status(404).send({
            message: 'Farmer Not Found',
          });
        }
        Culture
          .findAll({
            where: { id: req.body.sales }
          })
          .then((cultures) => {
            if (!cultures) {
              return res.status(404).send({
                message: 'Culture Not Found',
              });
            }
            farmer.removeSales(cultures);
            return res.status(200).send(farmer);
          })
      })
      .catch((error) => res.status(400).send(error));
  },
};
