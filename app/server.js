const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// config initials
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

// routes of project
app.use("/api", require("./farmer/router"));
app.use("/api", require("./address/router"));
app.use("/api", require("./culture/router"));

// server init
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
