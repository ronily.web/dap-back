"use strict";

module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define("Address", {
    county: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    area: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    proximity: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  }, {});
  Address.associate = function(models) {
    Address.hasOne(models.Farmer, {
      foreignKey: 'address_id',
      as: 'address_id',
    });
  };
  return Address;
};
