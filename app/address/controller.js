const { Address } = require("../sequelize");

module.exports = {
  // lista os endereços
  getAll(req, res) {
    return Address
      .findAll({
        attributes: ['county', 'area', 'proximity']
      })
      .then((addresses) => res.status(200).send(addresses))
      .catch((error) => res.status(200).send(error))
  },

  // busca um endereco pela sua chave primaria
  getByPk(req, res) {
    return Address
      .findByPk(req.params.id, {
        attributes: ['county', 'area', 'proximity']
      })
      .then((address) => res.status(200).send(address))
      .catch((error) => res.status(200).send(error))
  },

  //atualiza um endereço
  update(req, res) {
    return Address
      .findByPk(req.params.id)
      .then(address => {
        if (!address) {
          return res.status(404).send({
            message: 'Address Not Found',
          });
        }
        return address
          .update({
            county: req.body.county,
            area: req.body.area,
            proximity: req.body.proximity,
          })
          .then((address) => res.status(200).json(address))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};
