const router = require("express").Router();
const controller = require("./controller");

router.get("/addresses", controller.getAll);
router.get("/addresses/:id", controller.getByPk);
router.put("/addresses/:id", controller.update);

module.exports = router;
