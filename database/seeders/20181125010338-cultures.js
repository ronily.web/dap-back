'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Cultures', [
      {
        name: 'Feijão',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Mandioca',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Batata',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Arroz',
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Cultures', null, {});
  }
};
